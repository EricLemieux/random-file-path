# Random file path

Selects a random file inside of the given path.

Uses async await with `fs-extra` for asynchronous file system searching which returns a promise to the file path.

## Test
`npm run test`  
`npm run test:watch`  
`npm run test:coverage`  

## Lint
`npm run lint`  
`npm run lint:fix` 
