const fs = require('fs-extra');

const randomFilePath = async (path) => {
  try {
    const exists = await fs.exists(path);

    // Path does not exist.
    if (!exists) {
      return null;
    }

    const stats = await fs.stat(path);
    const isDir = stats.isDirectory();

    // Path is not a directory, so just return the path.
    if (!isDir) {
      return path;
    }

    const files = await fs.readdir(path);

    // No files exist in the directory.
    if (files.length <= 0) {
      return null;
    }

    return files[Math.floor(Math.random() * Math.floor(files.length))];
  } catch (err) {
    return null;
  }
};

module.exports = randomFilePath;
