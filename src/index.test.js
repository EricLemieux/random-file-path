const fs = require('fs-extra');
const randomFilePath = require('./index');

const exampleDirPath = 'example';

test('Path that does not exist', async () => {
  const pathThatDoesNotExist = await randomFilePath('path-that-does-not-exist');
  expect(pathThatDoesNotExist).toBe(null);
});

test('Path that is not a directory', async () => {
  const pathThatIsNotADirPath = 'package.json';
  const pathThatIsNotADir = await randomFilePath(pathThatIsNotADirPath);
  expect(pathThatIsNotADir).toBe(pathThatIsNotADirPath);
});

test('Path is a directory that is empty', async () => {
  const exists = await fs.exists(exampleDirPath);

  // Path already exists, so need to clean up first.
  if (exists) {
    fs.remove(exampleDirPath);
  }

  await fs.mkdir(exampleDirPath);
  const pathThatIsEmptyDir = await randomFilePath(exampleDirPath);
  expect(pathThatIsEmptyDir).toBe(null);
});

test('Path is a directory that has files', async () => {
  const file1 = 'file1';
  const file1Path = `${exampleDirPath}/${file1}`;
  await fs.writeFile(file1Path, '');
  const pathThatIsFullDir = await randomFilePath(exampleDirPath);
  expect(pathThatIsFullDir).toBe(file1);

  // Clean up test files.
  fs.remove(exampleDirPath);
});
